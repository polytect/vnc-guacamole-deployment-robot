#!/bin/sh
# VNC Guacamole Deployment Robot 18.06.2024

default='\e[0m'
black='\e[0;30m'
white='\e[0;37m'
yellow='\e[0;33m'
red='\e[0;31m'
green='\e[0;32m'
blue='\e[0;34m'

#[FAILED]   "
#[EXIT]     "
#[QUESTION] "
#[TRY]      "
#[ASK]      "
#[FILL]     "
#[NOTE]     "
#[SUCCESS]  "
#[OK]       "
#[EXECUTE]  "

FAILED="[${red}FAILED${default}]  "
EXIT="[${red}EXIT${default}]    "

QUESTION="[${yellow}QUESTION${default}]"
TRY="[${yellow}TRY${default}]     "
ASK="[${yellow}ASK${default}]     "
NOTE="[${yellow}NOTE${default}]    "
FILL="[${yellow}FILL${default}]    "
EXECUTE="[${yellow}EXECUTE${default}] "

SUCCESS="[${green}SUCCESS${default}] "
OK="[${green}OK${default}]      "

# Grab the current username
username=$(whoami)
echo -e "${OK} Current user is '${username}'."
echo -e "${TRY} If '${username}' has sudo privileges."
# Check if the current user has sudo privileges
if sudo -v >/dev/null 2>&1; then
    echo -e "${OK} Current user '${username}' has sudo privileges."
else
    echo -e "${FAILED} Current user '${username}' does not have sudo privileges."
    echo -e "${QUESTION} Try to fix sudo privileges first? Type (Y/y). (N/n) for No and doing it yourself:"
    read -r response
    if [ "$response" = "Y" ] || [ "$response" = "y" ]; then
        echo -e "${TRY} Fixing sudo permissions using root."



        # Ask for the root password
        echo -ne "${ASK} Root password: "
        read -s root_password
        echo

        # run command as the root user to add user to sudo
        chmod +x ./scripts/add_user_to_sudo.sh
        bash ./scripts/add_user_to_sudo.sh $username $root_password &

        echo -e "${NOTE} Sudo will work in new shell enviroment."
        echo -e "${SUCCESS} Logout and Login or run: 'newgrp sudo' and try again."
        echo -e "${EXIT}"
        echo
        exit 1

    else
        echo -e "${FAILED} Sudo permissions not fixed."
        exit 1
    fi
fi


postgres_password=""
get_postgres_password() {
    local prompt1="$1"
    local prompt2="$2"
    local password
    local password_confirmation

    while true; do
        password=""
        password_confirmation=""

        # Read password without displaying input
        read -s -p "$prompt1" password
        echo

        # Read password confirmation without displaying input
        read -s -p "$prompt2" password_confirmation
        echo

        # Check if the passwords match and have a minimum length
        if [ -z "$password" ]; then
            echo -e "${FAILED} Passwords cannot be empty, please try again."
        elif [ "$password" != "$password_confirmation" ]; then
            echo "${FAILED} Passwords do not match, please try again."
        elif [ ${#password} -lt 10 ]; then
            echo "${FAILED} Passwords must have at least 10 characters, please try again."
        else
            echo "${OK} Passwords are OK."
            postgres_password=$password
            break
        fi
    done
}

echo "Would you like to generate a random password for PostgreSQL? (y/n)"
read -r generate_password

if [ "$generate_password" = "y" ] || [ "$generate_password" = "Y" ]; then
    # Generate a random password
    postgres_password=$(openssl rand -base64 16 | tr '/' 'a' | tr '+' 'b')
    echo "Generated PostgreSQL password: $postgres_password"
else
    # Prompt for a custom password
    get_postgres_password "Enter New PostgreSQL password: " "Confirm New PostgreSQL password: "  3>&1 >/dev/tty
    echo "Created PostgreSQL password: $postgres_password"
fi

# Prompt for domain name
# read -p "Please enter your domain name (e.g. gateway.exampledomain.com): " domain_name

# Remove any spaces from the domain name
# domain_name=$(echo "$domain_name" | tr -d ' ')

# Output the cleaned domain name
# echo -e "${OK} Domain Name: $domain_name"

vnc_password=""
get_vnc_password() {
    local prompt1="$1"
    local prompt2="$2"
    local password
    local password_confirmation

    while true; do
        password=""
        password_confirmation=""

        # Read password without displaying input
        read -s -p "$prompt1" password
        echo

        # Read password confirmation without displaying input
        read -s -p "$prompt2" password_confirmation
        echo

        # Check if the passwords match and have a minimum length
        if [ -z "$password" ]; then
            echo -e "Passwords cannot be empty, please try again."
        elif [ "$password" != "$password_confirmation" ]; then
            echo "Passwords do not match, please try again."
        elif [ ${#password} -lt 6 ]; then
            echo "Passwords must have at least 6 characters, please try again."
        else
            echo "Passwords are OK."
            vnc_password=$password
            break
        fi
    done
}

# ASK FOR TIGERVNC NEW PASSWORD ----------------------------------------#
get_vnc_password "Enter New VNC password: " "Confirm New VNC password: " 3>&1 >/dev/tty


#-----------------------------------------------------------------------#
# PREPARING TO CONFIGURE DOCKER-COMPOSE.YML ----------------------------#
#-----------------------------------------------------------------------#

echo -e "${TRY} Copy ./deployment_templates/docker-compose-for-reverse-proxy.yml to ./docker-compose.yml"
cp ./deployment_templates/docker-compose-for-reverse-proxy.yml ./docker-compose.yml
sudo sed -i "s/ChooseYourOwnPasswordHere1234/$postgres_password/g" ./docker-compose.yml
echo -e "${OK} For File ./docker-compose.yml add New PostgreSQL password."


#-----------------------------------------------------------------------#
# INSTALL/REINSTALL PACKAGES VNC, XFCE4, UFW ---------------------------#
#-----------------------------------------------------------------------#


# Update system packages
echo -e "${EXECUTE} Update system packages"
sudo apt update


# Install XFCE4 and related packages
echo -e "${EXECUTE} Install XFCE4 and related packages"
sudo apt install xfce4 xfce4-goodies dbus-x11 -y


# Install TigerVNC server
echo -e "${EXECUTE} Install TigerVNC server packages"disabled.
sudo apt install tigervnc-standalone-server tigervnc-common -y


# Install Additional Packages
echo -e "${EXECUTE} Install Additional Packages"
sudo apt install expect ufw -y


#-----------------------------------------------------------------------#
# INSTALL/REINSTALL DOOCKER PACKAGES -----------------------------------#
#-----------------------------------------------------------------------#

# Install dependencies
echo -e "${EXECUTE} Install Additional Packages"
sudo apt install  apt-transport-https ca-certificates curl software-properties-common -y


# Add Docker's official GPG key

if [ -f "/usr/share/keyrings/docker-archive-keyring.gpg" ]; then
    echo -e "${NOTE} Docker keyring file already exists. Skipping the command."
else
    echo -e "${EXECUTE} Add Docker's official GPG key"
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo -e "${OK} Docker keyring file created."
fi


# Add Docker's stable repository
echo -e "${EXECUTE} Add Docker's stable repository"
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null

# Update the package lists (again) with Docker repository
echo -e "${EXECUTE} Update the package lists (again) with Docker repository"
sudo apt update

# Install Docker
echo -e "${EXECUTE} Install Docker"
sudo apt install docker-ce docker-ce-cli containerd.io -y

#-----------------------------------------------------------------------#
# SETUP OF TIGERVNC SERVICE --------------------------------------------#
#-----------------------------------------------------------------------#

# Set set_vncpasswd.sh executable permissions
chmod +x ./scripts/set_vncpasswd.sh
# Run the set_vncpasswd.sh script with the obtained password using expect
./scripts/set_vncpasswd.sh "$vnc_password"
echo -e "${OK} Run the set_vncpasswd.sh script with the obtained password using 'expect'"


# Create a new xstartup file
cat << EOF > ~/.vnc/xstartup
#!/bin/sh
# Start up the standard system desktop
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
/usr/bin/startxfce4
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
x-window-manager &
EOF
echo -e "${OK} Create a new xstartup file in ~/.vnc/xstartup"


# Set xstartup executable permissions
chmod +x ~/.vnc/xstartup
echo -e "${OK} Set xstartup executable permissions for ~/.vnc/xstartup"

echo -e "${TRY} Kill VNC server if exist"
# Kill VNC server if exist
vncserver -kill :1


# Find tigervnc-server@.service file in deployment_scripts/ directory
service_file=$(find ./deployment_templates/ -name "tigervnc-server@.service" 2>/dev/null | head -n 1)

if [ -z "$service_file" ]; then
    echo -e "${FAILED} tigervnc-server@.service file not found."
    exit 1
fi


# Copy the file to /etc/systemd/system
sudo cp "$service_file" /etc/systemd/system/
echo -e "${OK} Copy 'tigervnc-server@.service' to /etc/systemd/system/"


# [OLD] Replace contents of the file with the username
# sudo sed -i "s/username/$username/g" /etc/systemd/system/tigervnc-server@.service
# echo -e "${OK} Rename contents of 'tigervnc-server@.service'"


echo -e "${TRY} Enable service 'tigervnc-server@${username}.service'"
# Enable and start, restart the service
sudo systemctl enable "tigervnc-server@${username}.service"
sudo systemctl start "tigervnc-server@${username}.service"
sudo systemctl restart "tigervnc-server@${username}.service"


#-----------------------------------------------------------------------#
# UFW CONFIGURATION ----------------------------------------------------#
# BLOCK ALL PORTS EXEPT FROM `172.0.0.0\16` ----------------------------#
#-----------------------------------------------------------------------#



if [ "$(sudo which ufw)" != "" ]; then
    echo -e "${OK} UFW Firewall is installed"

    # Disable UFW
    sudo ufw disable
    echo -e "${OK} UFW is disabled."

    # Reset UFW
    sudo ufw --force reset
    echo -e "${OK} UFW Reset / Save Old Firewall rules"

    # Set default policies
    sudo ufw default deny incoming
    echo -e "${OK} Default policies set: deny incoming traffic."

    # Allow SSH port for incoming traffic
    sudo ufw allow 22
    echo -e "${OK} Allowed incoming traffic on SSH port (22)."

    # Allow SSH port for incoming traffic
    sudo ufw allow 8080
    echo -e "${OK} Allowed incoming traffic on port HTTP (8080)."

    # Allow SSH port for incoming traffic
    sudo ufw allow 8443
    echo -e "${OK} Allowed incoming traffic on port HTTPS (8443)."

    # Allow incoming connections on VNC-related ports from the 172 network (TCP and UDP)
    sudo ufw allow from 172.59.0.0/16 to any port 5900:5909 proto tcp
    echo -e "${OK} Allowed incoming TCP connections on VNC-related ports (5900-5909) from the 172.59.0.0/16 network."
    sudo ufw allow from 172.59.0.0/16 to any port 5900:5909 proto udp
    echo -e "${OK} Allowed incoming UDP connections on VNC-related ports (5900-5909) from the 172.59.0.0/16 network."

    # Enable UFW
    sudo ufw enable
    echo -e "${OK} UFW is enabled."

else
    echo -e "${FAILED} UFW is not installed."
    exit
fi

#-----------------------------------------------------------------------#
# SETUP OF DOCKER-COMPOSE ----------------------------------------------#
#-----------------------------------------------------------------------#
# Install Docker Compose
sudo curl -sSL https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo -e "${NOTE} # Docker versions installed -------#"
# Verify the installation
docker --version
docker-compose --version
echo "# --------------------------------------------#"

echo -e "${TRY} systemctl start docker"
sudo systemctl start docker
sudo systemctl enable docker

sudo usermod -aG docker $username

#-----------------------------------------------------------------------#
# DOCKER SETUP OF GUACAMOLE --------------------------------------------#
#-----------------------------------------------------------------------#

# Check if Docker is running
if [[ $(systemctl is-active docker) == "active" ]]; then
    echo -e "${OK} Docker daemon is running."
else
    echo -e "${FAILED} Docker daemon is not running. Exiting..."
    exit
fi

echo -e "${TRY} Preparing folder init and creating ./init/initdb.sql"
mkdir ./init >/dev/null 2>&1
mkdir -p ./nginx/ssl >/dev/null 2>&1
chmod -R +x ./init
sudo docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --postgresql > ./init/initdb.sql

echo -e "${SUCCESS} docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --postgresql > ./init/initdb.sql"
echo -e "${TRY} Creating SSL certificates"
# Example of -subj " "/C=DE/ST=BY/L=Hintertupfing/O=Dorfwirt/OU=Theke/CN=subdomain.mydomain.com/emailAddress=docker@mydomain.com"
openssl req -nodes -newkey rsa:2048 -new -x509 -days 3650 -keyout nginx/ssl/self-ssl.key -out nginx/ssl/self.cert -subj '/C=UN/ST=UN/L=Unknown/O=Unknown/OU=Unknown/CN=Unknown/emailAddress=Unknown'

echo -e "${OK} Certificate generated to: ginx/ssl/self-ssl.key and nginx/ssl/self.cert"
echo -e "${NOTE} You can use your own certificates by placing the private key in nginx/ssl/self-ssl.key and the cert in nginx/ssl/self.cert"

echo -e "${TRY} To run sudo docker-compose down"
sudo docker-compose down
echo -e "${TRY} To run sudo docker-compose up -d"
sudo docker-compose up -d
echo -e "${SUCCESS} done"
