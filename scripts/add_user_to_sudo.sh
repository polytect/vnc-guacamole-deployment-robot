#!/bin/bash

# Check if a username was provided
if [ -z "$1" ]; then
    echo "No username provided. Usage: $0 <username> <password>"
    exit 1
fi

# Check if a password was provided
if [ -z "$2" ]; then
    echo "No password provided. Usage: $0 <username> <password>"
    exit 1
fi

username=$1
password=$2

# Check if user exists
if id "$username" >/dev/null 2>&1; then
    # Run command as the root user to add user to sudo
    echo $password | su -c "usermod -aG sudo $username" - root
else
    echo "User $username does not exist."
    exit 1
fi
