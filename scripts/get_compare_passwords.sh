get_password() {
    local prompt1="$1"
    local prompt2="$2"
    local password
    local password_confirmation

    while true; do
        password=""
        password_confirmation=""

        # Read password without displaying input
        read -s -p "$prompt1" password
        echo

        # Read password confirmation without displaying input
        read -s -p "$prompt2" password_confirmation
        echo

        # Check if the passwords match
        if [ -z "$password" ]; then
            echo "Passwords cannot be empty, please try again."
        elif [ "$password" == "$password_confirmation" ]; then
            echo "Passwords are OK."
            break
        else
            echo "Passwords do not match, please try again."
        fi
    done
}

# Call the function and store the result in a variable
password=$(get_password "Enter VNC password: " "Enter VNC password again: " 3>&1 >/dev/tty)
echo "Password confirmed: $password"
