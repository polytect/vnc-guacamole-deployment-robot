#!/usr/bin/expect -f

set password [lindex $argv 0]
set timeout -1

spawn vncpasswd
expect "Password:"
send -- "$password\r"
expect "Verify:"
send -- "$password\r"
expect "Would you like to enter a view-only password (y/n)?"
send -- "n\r"
expect eof
