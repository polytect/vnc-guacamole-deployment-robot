# VNC Guacamole Deployment Robot

## Streamlined Apache Guacamole & TigerVNC Deployment on Debian

Driven by the frustration of manual deployment, I've developed an automated system for deploying **Apache Guacamole** and **TigerVNC**. Harnessing **Docker**'s consistency, **Guacamole** and **TigerVNC**'s efficiency are highlighted, offering browser-based remote desktop access.

Our setup integrates **NGINX** for **SSL**, **PostgreSQL** for **Guacamole** user management, and **Guacd** to bridge to a host **TigerVNC** server from **Docker** network.

This enables to access your server via **HTTPS** from everywhere in the world!

![](./images/screenshot.png)

## Quick Install:

```bash
git clone "https://gitlab.com/polytect/vnc-guacamole-deployment-robot.git"
cd vnc-guacamole-deployment-robot
bash ./deploy.sh
```

## Usage

Easy installation is the cornerstone of this design. Run `bash ./deploy.sh` and follow the intuitive steps. You'll confirm sudo access, choose PostgreSQL password options, and create a TigerVNC password. The rest is automated.

If the server is configured properly you should see this using `nmap localhost` (install **nmap** if required):
```bash
PORT      STATE SERVICE
25/tcp    open  smtp
631/tcp   open  ipp
5901/tcp  open  vnc-1
8443/tcp  open  https-alt
32768/tcp open  filenet-tms
```

## IMPORTANT

Access: `https://serverip:8443`
Default password for Apache Guacamole:
- Username: `guacadmin`
- Password: `guacadmin`

**Please change it immediately**

## Tested On

Debian 12

## Roadmap

- ArchLinux Support
- Fedora Support
- Enhanced options
- Nginx Proxy Manager

## Acknowledgment

Author: Tautvydas Vaitkus (aka. polytect)

## License

**GPLv3** \- See the **LICENCE** file

## Project status

Steady, incremental development.
